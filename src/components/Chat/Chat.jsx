import React from 'react';
import Header from '../Header/Header';
import MessageInput from '../MessageInput/MessageInput';
import MessageList from '../MessageList/MessageList';
import './Chat.css';
import { v4 as uuidv4 } from 'uuid';
import Preloader from '../Preloader/Preloader';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            id: '',
            user: "Denis",
            text: '',
            createdAt: '',
            editedAt: '',

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleChange(event) {
        this.setState({ text: event.target.value });
    }

    handleSubmit(event) {
        this.setState(state => {
            let { id, text, user, createdAt, editedAt, items } = state;
            createdAt = new Date().toISOString();
            id = uuidv4();

            if (text !== '') {
                const message = { id, text, user, createdAt, editedAt }

                event.target.reset();
                event.preventDefault();
                return {
                    text: '',
                    items: [...items, message]
                };
            }
            event.preventDefault();
        });
    }

    handleDelete(itemId) {
        const { items } = this.state;
        const filtered = items.filter(item => item.id !== itemId);
        this.setState({ items: filtered })
    }

    getUsersCount(items) {
        const username = items.map(user => user.user);

        return Array.from(new Set(username)).length;
    }

    getMessageCount(items) {
        const messagesCount = items.length;

        return messagesCount;
    }

    getLastMessageFullDate(items) {
        const lastMessageDateISO = items[items.length - 1].createdAt;
        const date = `${lastMessageDateISO.substring(8, 10)}.${lastMessageDateISO.substring(5, 7)}.${lastMessageDateISO.substring(0, 4)}`;
        const hoursISO = `${lastMessageDateISO.substring(11, 13)}`;
        const hours = parseInt(hoursISO, 10) + 3;
        const minutes = `${lastMessageDateISO.substring(13, 16)}`;
        const lastMessageFullDate = `${date} ${hours}${minutes}`;

        return lastMessageFullDate;
    }

    componentDidMount() {
        fetch(this.props.url)
            .then(res => {
                return res.json()
            })
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (!items) {
            return <Preloader />
        }
        if (error) {
            return <div>Ошибка: {error.message}</div>;
        } else if (!isLoaded) {
            return <Preloader />;
        } else {
            const usersCount = this.getUsersCount(items);
            const messagesCount = this.getMessageCount(items);
            const lastMessageFullDate = this.getLastMessageFullDate(items);

            return (
                <div className="chat">
                    <Header
                        usersCount={usersCount}
                        messagesCount={messagesCount}
                        lastMessageFullDate={lastMessageFullDate}
                    />
                    <MessageList items={items} onDelete={this.handleDelete} />
                    <MessageInput items={items} text={this.state.text} onSubmit={this.handleSubmit} onChange={this.handleChange} />
                </div>
            );
        }
    }
}

export default Chat;