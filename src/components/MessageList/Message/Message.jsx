import { faHeart } from '@fortawesome/free-regular-svg-icons';
import * as fasHeart from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import './Message.css';

class Message extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLike: false
        }
        this.handleLike = this.handleLike.bind(this);
    }

    handleLike() {
        const { isLike } = this.state;
        if (!isLike) {
            this.setState({ isLike: true })
        } else this.setState({ isLike: false })
    }

    render() {
        const { text, time, username, avatar } = this.props
        const { isLike } = this.state;

        return (
            <div className="message">
                <div className="message-text">{text}</div>
                <div className="message-time">{time}</div>
                <div className="message-like" onClick={this.handleLike}>
                    {isLike
                        ? <FontAwesomeIcon icon={fasHeart.faHeart} />
                        : <FontAwesomeIcon icon={faHeart} />
                    }
                </div>
                <div className="message-user-name">{username}</div>
                <div className="message-user-avatar-block">
                    <img className="message-user-avatar" src={avatar} alt={avatar} />
                </div>
            </div>
        );
    }
}

export default Message
