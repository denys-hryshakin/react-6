import React from 'react';
import Message from './Message/Message';
import OwnMessage from './OwnMessage/OwnMessage';
import './MessageList.css';

class MessageList extends React.Component {

    render() {
        const { items, onDelete, onLike, isLike } = this.props;

        const MessageElement = items.map(m => {
            const messageHoursISO = m.createdAt.substring(11, 13);
            const minutes = m.createdAt.substring(13, 16);
            const time = `${parseInt(messageHoursISO, 10) + 3}${minutes}`;

            return m.user === 'Denis'
                ? <OwnMessage
                    key={m.id} id={m.id} text={m.text} time={time} onDelete={onDelete} />
                : <Message
                    key={m.id} id={m.id} text={m.text} time={time}
                    username={m.user} avatar={m.avatar} isLike={isLike} onLike={onLike} />
        });

        return (
            <div>
                <div className="message-list">
                    {MessageElement}
                </div>
            </div>

        );
    }
}

export default MessageList;
