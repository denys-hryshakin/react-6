import React from 'react';
import './OwnMessage.css';

class OwnMessage extends React.Component {

    delete = () => {
        const { id, onDelete } = this.props;
        onDelete(id);
    };

    render() {
        const { time, text } = this.props;
        return (
            <div className="own-message">
                <div className="message-text">{text}</div>
                <div className="message-time">{time}</div>
                <button type="button" className="message-edit">Edit</button>
                <button type="button" className="message-delete" onClick={this.delete}>Delete</button>
            </div>
        );
    }
}

export default OwnMessage
