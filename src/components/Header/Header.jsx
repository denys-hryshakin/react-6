import React from 'react';
import './Header.css';
import { faUserFriends, faComments, faClock } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Header = ({ usersCount, messagesCount, lastMessageFullDate }) => {
    return (
        <div className="header">
            <div className="header-container">
                <div className="header-title">Ozark</div>
                <div className="header-users-count"><FontAwesomeIcon icon={faUserFriends} /> {usersCount} participants</div>
                <div className="header-messages-count"><FontAwesomeIcon icon={faComments} /> {messagesCount} messages</div>
                <div className="header-last-message-date"><FontAwesomeIcon icon={faClock} /> last message at {lastMessageFullDate}</div>
            </div>
        </div>
    );
}

export default Header;
