import React from 'react';
import './MessageInput.css';

class MessageInput extends React.Component {

    render() {

        return (
            <form className="message-input" onSubmit={this.props.onSubmit} >
                <input
                    className="message-input-text"
                    type="text"
                    value={this.props.value}
                    onChange={this.props.onChange}
                    placeholder="Enter The Message" />
                <input className="message-input-button" type="submit" value="Send"></input>
            </form>
        );
    }
}

export default MessageInput;
